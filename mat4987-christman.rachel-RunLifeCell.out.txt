*** Life<Cell> 12x10 ***

Generation = 0, Population = 19.
0-0-------
----------
0----00---
----------
----0-----
------0---
0-----0--0
00--------
-0--------
---0-0---0
----------
-0-0-----0

Generation = 4, Population = 44.
--0--0----
------1---
-01-1-1.--
00----*---
-00--*--0-
1-11-0---1
.*--1-1**-
0-*01*---1
-1--11---*
---1-00---
0.----0--1
0--0-0---0

*** Life<Cell> 11x12 ***

Generation = 0, Population = 18.
---------0--
------0-----
0--------0--
---0--------
0-----------
--------0-0-
---0---0-0--
--0-------0-
----------0-
---0---0---0
---0--------

Generation = 3, Population = 63.
-0-00-0---0-
0--110-0----
----00----0-
0-00011--0--
----000-0-0-
0---00----00
-1--0--101*-
1-01100--0.-
00000-0--*1-
0-*--0-00--1
010.--0--01-

Generation = 6, Population = 62.
--1-1--0----
0-*---010---
01-0-.1*--0-
0-10-***-0*0
---*.-001*.-
10-*--1-1--*
*--000-.0..-
.--.-1-*00.1
.11.1----*.-
1-.*-00.10**
0--.--0101*-

*** Life<Cell> 10x12 ***

Generation = 0, Population = 17.
------------
--0-00-0-0--
---0-0-0----
--0---------
----0--0--0-
---------0--
--------0---
------0-----
------------
----0---0---

Generation = 1, Population = 40.
--0-00-0-0--
-0-01--1--0-
--0-01-100--
-0--00----0-
--00-00-0--0
----0--0----
------00----
-----0-00---
----0-0-0---
---0-0-0-0--

Generation = 2, Population = 38.
---0-1-1----
0--1*00--0-0
-0-01----10-
0-0--1-0----
--11--10-0--
--0---01---0
----0-11----
---------0--
--------1---
--0-0-0-0-0-

Generation = 3, Population = 65.
0-010*0-00-0
--0*.11-010-
0-0--1010-1-
-010--010-0-
00**0--1--00
-01-001*000-
--00----00-0
----0-00--0-
--0-0-00*-0-
-0------1--0

Generation = 4, Population = 42.
---*-.----0-
-01..**11*--
-0-01--*11-0
0-----------
1-..--1-0-11
--*----.11-0
---100-1----
----1---0-1-
----1--1*-1-
0---0-0-*0--

Generation = 5, Population = 63.
-0-.0.010--0
0-*..****.-0
-101*--.*---
1--00-01-00-
*0..-0--10*-
00.-001.-*01
---*11--0000
-------01-*0
0--0-0-*.0*0
-0-01--0*1--

Generation = 6, Population = 46.
---.-.-*-001
--.......*01
0*1*.--..-10
*0---11*---0
.-**011-*-.-
--*---**1.--
000**----1-1
0---1-0---.1
--011--..-.-
----*-0-.--0

Generation = 7, Population = 47.
---.-.0.--1-
00*.....*.-*
-.-..10..--1
*-1---**---1
.-**-**-*0.1
0-.0--*.-*-1
--1.*1-1---*
1--0---0-0**
001-*0-..-.-
--0-*--0.-0-

Generation = 8, Population = 54.
000.-.-.00--
1-...***..-*
-.-..*-..-1-
.-*--1..-00-
.0**-*.1*-.-
-0.1-0..-*--
-0-.**1*0--*
*-0-1-01-1**
---1*10.*0.0
0-1-*-0-.1-0

Generation = 9, Population = 56.
--1.-.0*1111
*-...*****-.
0.1.**-..-*-
.-*-0-..-1-1
.-.*-*.**0*-
0-.*0-..1*01
-1-...*.11-*
.0-0--1---.*
--1*.-1..1.-
---0*010**-1

Generation = 10, Population = 54.
00-.-*-.**-*
.0.**.....0*
1.*.*.-*.1*-
.0*011*.-*--
.-*.-*.*.-.1
--**1-*.*.1-
-*1***..--0*
.10110-11-..
--*..--..*.-
----*-*-**--

*** Life<Cell> 10x11 ***

Generation = 0, Population = 19.
-0-----0--0
--0-----0--
----------0
--------0--
0---0-0----
0---00-----
-0----00---
---0-------
--------0-0
-----------

Generation = 3, Population = 56.
----0-011--
10--101--01
11-0-01-10-
----*-----*
**-0*-0-*-1
*--00--*---
--0-01*--11
--0---00000
-1000-0*10-
-00-0--00--

Generation = 6, Population = 43.
---*1---.1*
*-----.0-0.
---1-1*-.0-
-110*0-11-*
..-1.--..0*
*--0-1**-00
--1*01.--*1
---11-...-0
-*-1---.---
-1--1---0--

Generation = 9, Population = 53.
1*-**01*.1.
*-00.*.1-1.
1*0*--.*.--
-.--.11*.-*
.*-1.1-*.-*
.0-1.-*.0.1
*-*..*..-.*
1.1.--**.-1
*.0.-0-*1-*
-**-1--0--0

*** Life<Cell> 10x12 ***

Generation = 0, Population = 19.
-----00-----
-0---0------
----0----0--
-----------0
--------0---
---0--------
------------
----0-0--0-0
-0----0---0-
0---00------

Generation = 4, Population = 44.
-01-0***0-1-
-0-1--0--1--
0-----0-.--1
----1-01-.-*
-0--*0---.*-
1*--1--1----
----1----0--
--*-1-.1.*0-
--0--1--1-0-
001.-.0-0--0

Generation = 8, Population = 49.
-0*-**..-.--
0.1-01----0-
----1*0-.10.
--0..*-.-.1*
-.-1.-*00..-
..-1*----..*
--1-.0*--*--
-..*.**-..-1
1---*1*0*1.1
.*1.1*-*111-

*** Life<Cell> 12x11 ***

Generation = 0, Population = 18.
--0-0-0----
----------0
------0----
------0----
-0---0-----
--0--0-----
-----0-----
------0---0
----0------
-----------
----0--0---
--------0-0

Generation = 4, Population = 45.
-10--0--*-1
--1--0--0-0
----0-*0--0
--*--------
--1--.0--0-
00----0--10
-0--1*0*-0-
--*-*------
01---..-0--
--0--1--0-0
0--0*1.10--
----1---0--

Generation = 8, Population = 57.
-**-0****-.
*-.*-**-1-0
--11-*.1*--
-0**.-.1.1-
1-*--....*0
.0**1---0.*
0.-0..-.1--
--.*.*-----
*.*-...1**.
-0*--.1-1--
--11*1...1*
0-0-.--1*--

*** Life<Cell> 10x12 ***

Generation = 0, Population = 18.
-0----------
-0------0---
------0-----
0---0-------
----0-------
00-------0--
------00----
-0----0-0--0
------------
-0--0-------

Generation = 2, Population = 39.
-*-0--0-----
1--00-1---0-
-1------1---
------1-00--
--0---00----
-*101-111---
-----1--01--
1--0--*-0-10
------10----
--110---0--0

Generation = 4, Population = 43.
-.--01*-----
1*--*0.-11--
-11---01.-1-
011-1-.----0
00------1---
-..-*--.1--0
--10--1--.01
1-1-10.1-1.-
----1-------
-1*--11--1-0

Generation = 6, Population = 48.
1*0*0**-1-0-
..-1.-.0..0-
-*-1--*..---
01-01-.11---
001----1.-00
-.*-*1-..10-
----11---.-1
-0.1--.-1-.-
--01.------1
-*.*1..111-0

Generation = 8, Population = 54.
**-.-**11---
*.-1.-.0*.--
-*---1..*--0
0**-*-.1*-1-
-*1-1--..---
-.*-.1-..*-*
01--1-*1**11
-0.*--.*1-.1
01-..01--1-*
-*.**...-*0*

Generation = 10, Population = 63.
*.-.1*..-110
*.11*0*0.*--
-.--01.*.110
**.-*0.*.110
-.*0*0-..11-
*.*-*--*..1.
-111--.*.*-*
--*.1-.*1*.-
-.**.**1-.-*
-...***.**0*

*** Life<Cell> 12x12 ***

Generation = 0, Population = 19.
-0----------
---0--------
-0-------00-
---------0--
---------0--
-0--0------0
-----00--0--
------------
0-0------0--
------------
0----0------
------0-----

Generation = 4, Population = 51.
-----1----*-
-*-01--1---1
---*-1---011
-----0-----0
1*0--*---**1
10-1-0.010--
0-0--------1
------1--1--
0-0-*00--0-*
-0--1-*----1
001-0-1-----
--0-------0-

*** Life<Cell> 11x11 ***

Generation = 0, Population = 17.
-----0--0-0
--0--00---0
-----0-0---
00----0--0-
0----------
-----------
-----------
----0------
-----------
--00-------
-----------

Generation = 1, Population = 41.
--0-01-0--1
-0-0011-001
000-01--000
-10-----0-0
1-----0--0-
0----------
----0------
---0-0-----
--000------
-0110------
--00-------

Generation = 2, Population = 38.
---0--0---*
-1-1--*0-1*
1--0---0111
0--000001-1
*-0--0-0---
10--0-0--0-
0----------
---1--0----
-----------
01---0-----
-----------

Generation = 3, Population = 58.
-001011---*
----01*-0-.
*-0--101***
-1---1---0*
*0--01-1-00
*1-0-0--0-0
1--00----0-
0-0-00-0---
00-0-00----
1*1-0-0----
00---0-----

Generation = 4, Population = 42.
01--1*-000.
01----.01-*
.0-0-*1-..*
0*-------1.
.10-1*--0-1
.--1-10---1
-----0-0---
-0----0-00-
1----------
*.*1--10---
-----------

Generation = 5, Population = 63.
--01*.--11.
-*010-.--1*
.----.*-*.*
1.-00-0---*
*-10**-1-0*
.1--0*1-00*
-0-001---00
--0----0110
*-00---000-
.*.*00*10--
0-00--00---

Generation = 6, Population = 55.
---**.-0*-*
0*1*-1.-1-*
.00--..-..*
**---11-1-*
*1*1..--0-.
**-11.*01-.
11011--0011
0-11000-*--
.0---0-1---
*...--.----
-01--------

Generation = 7, Population = 51.
01-**.11*-.
1.*.0-.-*-*
.1---**-..*
..000**-*-.
.-.-..---0.
..-*-**1*-.
**1-*-0---*
1--*111-.-0
*--0-10----
*.*.-0.1---
-1*0-------

Generation = 8, Population = 55.
-*0****-*1.
*.*.1-.-*-.
.-000.*1...
..1--.*-.-.
.-*-..-10-.
.*-*1**-*-.
***-.---01.
---.***0.11
*0-10*----0
*...01.-0--
-**100-0---

Generation = 9, Population = 48.
-*1.**.-**.
....--.-*-.
.11--.**...
.*--0**0*-.
.-*-*.0*-0.
*.-*-**1.-.
***-.-----*
--1...*1.--
*1-*-.-10-1
*...1*.--00
-**-1------

Generation = 10, Population = 52.
0.*....1**.
...*11*-*-.
.-*00......
.*10-..-.-.
.-*1..1.0-.
*.0*1*.-*0.
*.*-.1---1.
-0**..*-*--
**0*-*--10*
*...-*.1-1-
-*.-*----00

*** Life<Cell> 10x11 ***

Generation = 0, Population = 18.
-0--0-0----
--000--00--
----------0
----0----0-
------0----
0-0--------
---------0-
--------0--
------0----
-----0-----

Generation = 2, Population = 39.
-000----1-0
00*-------1
-0--0---1-0
0-010-1-1--
----1---010
--0-----01-
-------0---
0-0--01---0
----0---10-
---0-0-00--

Generation = 4, Population = 39.
00*--00-1--
*-.---0---1
-*--*---*0-
0----0---0-
-00-.----**
0--1-0000--
-----1---11
-----*.--00
--1-------0
001-----00-

Generation = 6, Population = 71.
00****-011*
*1.-10---01
-.-1.011*0-
01*-10-0100
1*--.--11**
-001--00*--
-0-1-10-011
-01-1*.-10*
1-*-0-0-**-
*0-1000**--

Generation = 8, Population = 51.
-***..--1**
.-.1--11**1
0*0-.*1*.-*
0*.*.-**10-
-.-1.10*1..
---1--0-.*-
--0*-101**1
--1--..1.-.
*-.--0--..-
*--**-*..1-

Generation = 10, Population = 51.
1****.---..
.1..---...*
0.*-.*1..-*
0*...-*.*--
-.--.*0.1..
010-110**.0
-10*0.-*.**
1-.01*.*.1*
.-.---11*.1
.-***1...1-

*** Life<Cell> 11x12 ***

Generation = 0, Population = 19.
----0-------
--------0---
-----0------
0--------0--
---0------0-
-0---0------
-0--------0-
----0----0--
-----0------
----0--0-00-
---------0--

Generation = 3, Population = 65.
00--1--0-00-
-00---1-0*-0
-000---1--1-
1-10-----00-
-00-00-01---
-1-0--0-0---
010-0*---01-
0-10111----1
01--0-0*0--0
---011--10-*
--0--10*0.-1

Generation = 6, Population = 55.
-01-**11-01-
-----0-1**00
0-11.-----.0
1*.11*-1-*0-
-011-1-1-0--
---1---0*10-
-*--1.----*-
01---.-1--0.
-*-*-1-.----
**-1.1.-.-1*
-----1-*-.-.

*** Life<Cell> 11x12 ***

Generation = 0, Population = 19.
--0-----0---
------0-----
------------
---0--0--0--
----------0-
----0-------
-0-0-----00-
-0-----0--0-
0-------0---
--0---------
--------0---

Generation = 2, Population = 59.
0-0-0-1-0-0-
---00-0-10--
--0-----0-0-
-0---1011--0
-0-1----000-
-100---0-10-
1*-0-0-1--01
--1100--00--
0010--0-01--
11--0--0--0-
0-----0---0-

Generation = 4, Population = 52.
0---*-.---0-
--1-0-*-.-1-
-000----010-
-*0--.--*--0
1-1.--1---0-
0*0---1-1---
**----0*0-0*
-*---0---011
--*-----0-0-
*-0-0-1-----
*0-01---01*-

Generation = 6, Population = 65.
-11-.-.011--
10.-*-.1*1-1
100*-0-1--00
-*-0-.1-.0-0
.0-*--.1-*-0
*.0*-11-.*-1
..-001-.0--.
-*--1----*-1
1-.1---10**0
.*---01-01-0
**1-11-1*-.-

Generation = 8, Population = 56.
--*1.-.-.---
1-.1*0**.-*.
-0-.1-1*----
0.*--.-*.*--
**-.-1*10*-0
..-.-1.1**-.
.*0--10*-*-*
-.--*-0--.**
1*.1--0.*..-
*.1---.---**
.**1.--.*1*1

Generation = 10, Population = 52.
1-.1.1*-*--0
-1.-*-.*.*..
*00.*1-*1---
-.*-1.-*..11
*.1.-1.--.-0
*.*.11.*..**
..*11--.-.-.
-.-1*-0--..*
1..----*.*.-
*..-10*0-1..
.*.*.--..-.1

*** Life<Cell> 10x11 ***

Generation = 0, Population = 18.
0--------0-
----------0
--0--0-----
-----0-----
---00--00-0
-----0-----
-----------
0-0------0-
0--------0-
--------0--

Generation = 3, Population = 53.
----01--0--
----0*0-1-1
---0------*
-00--.--00*
----1-01.--
101-01-*---
*00--0011*1
-010-1-1--0
.--000-1010
*-10-0-01-1

Generation = 6, Population = 47.
---0----.-0
010-0..-.1*
0----.*-01.
-..*-.0---.
*-0..1-*.-*
11-*---.-*-
.0*1--011.-
-0.-01-*1-0
.-1.0*1.1-*
.-*1*1---**

*** Life<Cell> 12x12 ***

Generation = 0, Population = 18.
0-----------
-----0------
-----0------
---0--------
-------0--0-
------------
------0-----
-0---0------
0---------00
0-----------
----0---0---
0---0-----0-

Generation = 2, Population = 39.
--0--1------
-------0----
0--1------0-
-0----------
------1-11--
-0-0-0------
0---0--10--0
0------0----
*-0-0-0--1-*
-10-10----1-
0-0--------0
0---*-0-1-0-

Generation = 4, Population = 46.
--0-11-1--0-
-0---*---0--
00----0--1--
-0---0-0----
---1--1-..--
0----0------
--0-00--*-1-
0--000---10-
.-*-*-0--1-.
*.*-------1-
*-----1-----
-11-*00-1-0-

Generation = 6, Population = 59.
-0-0.---1-0-
-*---*-1---0
*----*0-1.10
0*---0-0----
1-1---1-*.0-
-11-0----01-
1------**1.-
011*--0-0-01
.-.-.1-1-***
.*.-*-0--0--
.-1-*-.-10--
-*.0.-001-0-

Generation = 8, Population = 51.
1--0.*--1-00
0.--1.-----*
*---1.-0-.*0
-.0-1---1001
*0*--0--..*1
1.--0*---0--
*--1---..-.1
-1-.-0--*1--
.0.-.--11...
...1.---0-11
.-1-.-.0-0--
-**-.--01---

*** Life<Cell> 12x11 ***

Generation = 0, Population = 16.
------00---
-0---------
------0----
---0-------
-----0-----
-----------
--0--------
-00--------
---0------0
---0-------
-0-0-------
---0-----0-

Generation = 3, Population = 61.
---01*-.-10
------0--0-
---01*--10-
--00-*0-0--
----0-*10--
-1*1-000--0
1---110--0-
*10010--0--
00-.0100-0-
------0---0
01-00-00-0-
0*0-01-----

Generation = 6, Population = 60.
01*.-.**---
-011-----0-
-11-1.-.*-0
10*-0.0--1-
10-.-0..-*-
-...1----1-
***.--1-010
..11*1-000-
-1-.1.***10
--1--00--0-
0--*0-0-10-
-.-1-.0--0-

Generation = 9, Population = 51.
.*..-..*..*
-1-*0-0.---
1*.*..-**--
-1.-1.-0-.0
---*01***.*
0*..-10-1-0
*...-*----*
.*--..1-1.*
--0......-*
*-*--..---0
11-...--.*0
-**.1.1*1--

*** Life<Cell> 12x12 ***

Generation = 0, Population = 17.
-0----------
--00--------
--0----0----
0-----0-----
-------00---
-----------0
------------
----0------0
0----0------
------------
---------0--
------0-0---

Generation = 4, Population = 47.
-00.-1--1---
0-01--*1---0
11-0--1-.--1
--------.-00
01-01*-.---0
--11---1-*--
--1----*-1--
-------1-*--
-00-0--01*--
------0-0--*
-----0---0-*
--0-------00

*** Life<Cell> 11x11 ***

Generation = 0, Population = 19.
---0-----0-
0--0--0----
--------0--
--0--------
-------0--0
0----------
0-00----0--
0-0--------
--------0--
-------0---
-------0---

Generation = 3, Population = 69.
-1-.01-1*10
1-01*--0*0-
-00*0--*0-0
10-*101-0-0
*---0--0*-1
1---0-00-00
-11----*10-
*0.01-0-0-0
--*1--1110-
1-1-01--01-
0-0-01-.--0

Generation = 6, Population = 47.
0--*1*-*..1
-1-**1**.10
0-..11-..-1
1-*.*1.1--1
*11-----.--
.-1-.-*.-1-
0-*---0..0-
.-*-.0.-1--
--*-*-*---0
.-1-011----
.---0--.--0

Generation = 9, Population = 50.
-*-...-....
.*0..*....1
.-*.***..*-
**...-.10*.
.*.*-*-**..
.-*0.*..-.-
--**-0-...*
*0*0.1.*---
***-.0.--*-
.0*11*-*-10
.--0-10*---

*** Life<Cell> 12x11 ***

Generation = 0, Population = 19.
0----------
--------0--
--00-0---0-
-----------
0----------
---------0-
0-0--------
0----------
--0--0-----
-00--------
---00---0--
---0-------

Generation = 3, Population = 70.
--0-----0--
00--00-0---
1*-.-1--000
000-0--01-0
0011--00-*0
10000-0-00-
*--01100-0-
**--00-0--0
0-.00-011--
-100-0-00-0
0--010---0-
--*.--0---0

Generation = 6, Population = 59.
01-0-*-0-*-
--1-.--0*--
..-.*.-*0--
11--10---10
10--*-1--.0
...--00--11
.---1**-011
.*0-*---1--
.*.010*--*1
0.--1.-01--
-*--*10-1-0
**..*1-*---

Generation = 9, Population = 65.
-*---*---.-
*111.-*.*--
*.*..**.-0-
-1*-.100-*0
111*.-**-*-
.*.-*0*-*-.
.-*01.*10*1
.**-*-0*.*-
.****0*-1.*
1.---.-.**1
-.0-...-.--
...*.*0*01-

*** Life<Cell> 11x12 ***

Generation = 0, Population = 17.
----0-------
0--------0--
------0-----
------------
---0------00
---0-----0--
-------0-0--
----------0-
---0--0-----
------------
--0--0-0----

Generation = 1, Population = 50.
0--0-0---0--
-0--0-0-0-0-
0----0-0-0--
---0--0---00
--010-----11
--010--001-0
---0--0--1--
---0--00---0
--0-00-0--0-
--00-000----
-0-00---0---

Generation = 2, Population = 42.
--0-0-------
0-0----0---0
---0----0-00
0--1--------
-0---0000--*
-0---0-1--01
-----010-*0-
---1----00--
-01-1---00--
----------0-
0-0-1-0--0--

Generation = 3, Population = 54.
001--0-0---0
--1-0-0----1
---10-----1-
--0*0000--0-
-1010--110-*
010-0-------
-0-00---0.--
-0---00--1--
01*0-0-0----
00----0-00-0
-------00---

Generation = 4, Population = 42.
1--0--0-0-01
0-*010-----*
--0-1--0-0*-
---.------1-
---*1----11.
1----0-1-1-1
--01--10-.--
------1---0-
--.1-1-----0
---0-----1--
00-----1---0

Generation = 5, Population = 68.
*0-1------1-
1-*1*1-0000*
0011*-0--1*-
--0.---0-0-0
0-0.*--11--*
-10---0*-*0-
001*0---0.00
--0---*--1--
--.*-----0--
000100-00-0-
1100--0-000-

Generation = 6, Population = 64.
*11*-0-00-*-
--.-.*01-1-*
---*.0-0-*.0
---.---10111
-1-..0-**1-*
1-1--01.-.-1
11-.10---.-1
0-1100.0-*-0
01..-100-1--
-11--101--10
-*1----1---0

Generation = 7, Population = 63.
.--.-10-10*-
10.-.*-*0*0*
--1.*10-0..-
-0-.--0*1--*
---..10..-1*
*--1-1**0.0-
**1.-1-0-.0-
--*-11.1-.-1
--..-*11-*--
-*-10-1*0-*-
1*-0-0-*0--1

Generation = 8, Population = 55.
.1-.0-10-1.-
*1.-..-.-.1*
00-.**---..-
0--.001.-1-*
01-.*-1..1-*
*----*..1.--
*.-.--1--*1-
0-*-**.-0.--
-1*.1.**0*-0
-*--1--.-1*-
**-11-0.-0--

*** Life<Cell> 10x11 ***

Generation = 0, Population = 20.
-----------
-00-----00-
-----00----
00---------
---------0-
0-0--------
-----------
--0---00--0
------000--
-------0-0-

Generation = 2, Population = 46.
-----00----
-*-10-11--1
---01--111-
-*10----01-
-----000---
00--0-00--0
------1100-
--0--1-0--0
----010-*01
--0--00*--1

Generation = 4, Population = 53.
-----*0--0-
-.--0-1-1-1
--0-----.10
**1-0111--1
-0---**001-
**--0-**0--
-----01----
-101-*-*11*
--01*-00.0*
0--1-0**-0.

Generation = 6, Population = 47.
--000*0-0-0
0*---1.11-*
1---1--**.*
*.1--.*-0*.
--0-1..0**-
*.1---..0-0
--1---1----
1.-1-.-.1..
1001.10*.-.
---.-**.--*

Generation = 8, Population = 46.
-0---*----*
-*---*.-*-*
---*--1*..*
.*.--.*1-*.
0---1**-..0
.*-11-***1-
-1.001-----
..-*-*-*-..
-0-1.1***1.
1-1.1*.*--.

*** Life<Cell> 11x12 ***

Generation = 0, Population = 18.
------0-0---
------------
----0-------
---0-0--0---
00-0------0-
---0-0----0-
------------
0-----------
--0-----0---
--------00--
------------

Generation = 1, Population = 48.
-----0---0--
----0-0-0---
--------0---
00010-00-00-
11--0---0010
0001--0--010
0--0-0----0-
-00-----0---
00-0---01---
--0----0-10-
--------00--

Generation = 2, Population = 47.
----------0-
---0-0--1---
0000----1-0-
-1----11----
**0010--1---
11-----0----
1001----0-1-
0-10-0--100-
1---0-0---0-
0-----01--10
--0-----1---

Generation = 3, Population = 71.
---0-0--00-0
00-1--00*0--
11--000-*--0
0-0-0-**0-0-
..---1-0-0--
**0-000-0-1-
*11-00--10*0
-0--0--0--10
-00--01-1---
100-00-*-1-1
00-0--0--000

Generation = 4, Population = 43.
0001--------
---*-0--.-0-
*-----1-.---
11--1-..-0--
*.--1*-1--1-
*.----101-*-
.---1-----.-
--1010--1---
-1-00-*---0-
---01--*0*1*
--0-----1---

Generation = 5, Population = 60.
1---00----0-
-0-*----.0-0
*--00--0.00-
**01*-*.0---
*.-0-*0*--*0
..----*1*-.0
.-1-*-00--.-
--*1-1-0-01-
1-0110*---1-
-0---0-*1.**
-0--0---*-00

Generation = 6, Population = 61.
----110-----
0--*00-0*1--
*1-1-0--.-1-
*.--.0..---0
*.-11.-.10*-
..010-.-*0*1
.1*1*0-1-0*0
00.--*-1-1-0
-1-*--.01---
-1-0110*-...
0100----*---

Generation = 7, Population = 56.
1----*1-00--
1-0*1101**--
**-*0-10*---
*.--.1..000-
.*---*-.-1.0
..--1-.-.-.-
*-.*.--*11.1
11.--*--1*11
---*--.1*--0
--01---.1...
1*---00-.0--

*** Life<Cell> 10x11 ***

Generation = 0, Population = 17.
------0----
---0--0----
-0-0-------
----00-----
------0----
---0---0000
0-------0--
-----0-----
-----------
----------0

Generation = 1, Population = 41.
---0-010---
-0010010---
0--1-00----
-0--11-----
---00---000
0-0-0--11-1
-0-0-0--1-0
0---0-0-0--
-----0----0
---------0-

Generation = 2, Population = 34.
-0-10-*-0--
-1------0--
-0-*-1-----
--00----000
00-11------
-0--1-0**0-
0-0-------1
--------10-
0-------0--
-----0--0--

Generation = 3, Population = 60.
01-*1-.-10-
0-0-001010-
010*--0--00
0011-1-01-1
1-0--000--0
01--*-1**1-
-0-00-00-0-
--0----0-1-
-0---0-0---
0---0-0010-

Generation = 4, Population = 46.
-*0*--*---0
--1---*-*1-
1*-.-1100--
11---*--*0-
--1---110-1
----*0*..--
-101---11-1
-0--00---*0
------0----
---0-011-10

Generation = 5, Population = 54.
0*1*10.01--
01*---.0**-
-.-.0-*-10-
----1*00.1-
1--110---0-
-1--*-...--
0--*0--**0*
01--11-0-*-
-0-0001----
--0--1**-*1

Generation = 6, Population = 54.
-.**-1*----
-*.--0.-..0
1*0*11.--10
1---*.11.-1
--1*---1011
0-00.-...-1
110*1-0*.1*
--00*-011*-
-1011--0--0
------.*-.*

Generation = 7, Population = 40.
-.*.--*0--0
-..---.-.*1
-.-**-.01*1
*--1..*-.1-
-0-.------*
1111.-*..1-
---.---..-*
011-.----.0
0*1*-0----1
-0000-..1..

Generation = 8, Population = 43.
-...-1.11-1
-..10-*-..*
1.0**1.1-.-
.-1**..-.*1
11-*-----1*
**-*.0...-1
---*--0..-*
-*--.1---*-
-.-.1-1-00*
--111-..-..
